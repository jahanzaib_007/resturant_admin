<div class="nano">
    <div class="nano-content">
        <ul>
            <li class="label">Main</li>
            <li class="#"><a href="{{route('dashboard')}}"><i class="ti-home"></i> Dashboard</a>

            </li>


            <li class="label">Restaurant</li>
            <li><a class="sidebar-sub-toggle"><i class="ti-cup"></i> Restaurant<span class="sidebar-collapse-icon ti-angle-down"></span></a>
                <ul>
                    <li><a href="restaurant-menu-one.html">Menu One</a></li>
                    <li><a href="restaurant-menu-two.html">Menu Two</a></li>
                    <li><a href="restaurant-menu-three.html">Menu Three</a></li>
                    <li><a href="restaurant-favourite-list.html">Favourite</a></li>
                    <li><a href="restaurant-order-list.html">Order List</a></li>
                    <li><a href="restaurant-booking.html">Booking</a></li>
                    <li><a href="restaurant-upload-menu.html">Upload Menu</a></li>
                </ul>
            </li>

        </ul>
    </div>
</div>
